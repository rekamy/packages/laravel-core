# Rekamy Laravel Core

<p>
<a href="https://packagist.org/packages/Rekamy/laravel-core">
<img class="total_img" src="https://poser.pugx.org/Rekamy/laravel-core/downloads">
</a>
<a href="https://github.com/Rekamy/laravel-core/blob/master/LICENSE">
<img class="license_img" src="https://poser.pugx.org/Rekamy/laravel-core/license">
</a>
</p>

## Installation

1. Add repository reference in composer.json file

    ```
        "repositories": [
            ...
            {
                "type": "gitlab",
                "url": "https://gitlab.com/rekamy/packages/laravel-core"
            }
            ...
        ]
    ```

2. Install the package

    ```
    composer require rekamy/laravel-core
    ```
