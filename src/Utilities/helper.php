<?php

use \Illuminate\Http\UploadedFile;

if (!function_exists('uploadFile')) {
    function uploadFile(UploadedFile $uploadedFile, $fileName = null, $folder = null, $disk = null)
    {
        $fileRef = [];
        $whitelistExtension = !empty(config('filesystems.whitelist'))
            ? explode(',', config('filesystems.whitelist.criteria.extension'))
            : ["jpg", "jpeg", "png", "bmp", "pdf"];
        $extension = strtolower($uploadedFile->getClientOriginalExtension());
        $whitelistExtensionList = collect($whitelistExtension)->map(fn ($ext) => strtoupper($ext))->join(' / ');
        if (!in_array($extension, $whitelistExtension)) abort(422, "Fail berformat " . strtoupper($extension) . " tidak dapat diproses. Sila muat naik fail berformat $whitelistExtensionList sahaja.");

        if (!$uploadedFile->getSize() || $uploadedFile->getSize() >= UploadedFile::getMaxFilesize()) abort(422, "Saiz fail terlalu besar. Sila muat naik fail bersaiz 100MB atau lebih kecil.");

        $fileRef['path'] = $disk ? $uploadedFile->store($folder, $disk) : $uploadedFile->store($folder);
        if (!$fileRef['path']) abort(500, "Gagal memuat naik fail.");
        $fileRef['name'] = empty($fileName) ? pathinfo($fileRef['path'], PATHINFO_FILENAME) : $fileName;

        // !!important: Image optimization required synchronous operation. resources does not exist yet
        // $fileRef['path'] = optimizeImage($fileRef['path']);

        // \App\Jobs\OptimizePdf::dispatch($fileRef['path']);

        return $fileRef;
    }
}

if (!function_exists('uploadFiles')) {
    function uploadFiles(array $uploadedFiles, $fileName = null, $folder = null, $disk = 'public')
    {
        $fileRefs = [];
        foreach ($uploadedFiles as $key => $uploadedFile) {
            $fileRefs[] = uploadFile($uploadedFile, $fileName . " $key", $folder, $disk);
        }
        return $fileRefs;
    }
}

if (!function_exists('ddf')) {
    function ddf(mixed ...$vars): never
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: *');
        header('Access-Control-Allow-Headers: *');
        http_response_code(500);
        dd($vars);
    }
}

use Illuminate\Foundation\Vite;
use Illuminate\Support\Facades\Vite as ViteFacade;

/**
 * support for vite
 */
function vite_proxy($module, $entrypoints = null): Vite
{
    return ViteFacade::useHotFile(resource_path($module . '/hot'))->useBuildDirectory('build-' . $module)->withEntryPoints([$entrypoints]);
}
