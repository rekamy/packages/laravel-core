<?php

namespace Rekamy\LaravelCore;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Log;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class LaravelCoreServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->registerHigherOrderContainer();
        $this->registerMacro();
        $this->bootstrapLoggingContext();
        $this->preventLazyLoading();
    }

    private function bootstrapLoggingContext()
    {
        Log::withContext([
            'server' => request()->server('HOSTNAME'),
            'env' => config('app.env'),
            'debug' => config('app.debug'),
            'url' => request()->url(),
            'method' => request()->method(),
            'payload' => request()->all(),
        ]);
    }

    private function preventLazyLoading()
    {
        if (!app()->runningInConsole() || app()->isLocal()) {
            Model::preventLazyLoading();
        } else if (!app()->isProduction()) {
            Model::handleLazyLoadingViolationUsing(function ($model, $relation) {
                $class = get_class($model);

                logger()->error("Attempted to lazy load [{$relation}] on model [{$class}].");
            });
        }
    }

    private function registerMacro()
    {
        \Rekamy\LaravelCore\Macro\Blueprint::register();
        \Rekamy\LaravelCore\Macro\Builder::register();
    }

    private function registerHigherOrderContainer()
    {
        $this->app->bind(
            \Illuminate\Pagination\LengthAwarePaginator::class,
            \Rekamy\LaravelCore\Override\LengthAwarePaginator::class
        );

        $this->app->bind(
            \Prettus\Repository\Criteria\RequestCriteria::class,
            \Rekamy\LaravelCore\Criteria\RequestCriteria::class
        );
    }
}
