<?php

namespace Rekamy\LaravelCore\Macro;

use Illuminate\Database\Schema\Blueprint as Base;

class Blueprint extends Base
{
    public static function register()
    {
        return new static;
    }

    public function __construct()
    {
        static::macro('auditable', function () {
            $this->datetime('created_at')->nullable();
            $this->datetime('updated_at')->nullable();
            $this->datetime('deleted_at')->nullable();
            $this->unsignedBigInteger('created_by')->nullable();
            $this->unsignedBigInteger('updated_by')->nullable();
            $this->unsignedBigInteger('deleted_by')->nullable();
        });

        static::macro('uuidAuditable', function () {
            $this->datetime('created_at')->nullable();
            $this->datetime('updated_at')->nullable();
            $this->datetime('deleted_at')->nullable();
            $this->uuid('created_by')->nullable();
            $this->uuid('updated_by')->nullable();
            $this->uuid('deleted_by')->nullable();
        });

        static::macro('is', function ($key, $prefix = 'is_') {
            return $this->boolean($prefix . $key);
        });
    }
}
