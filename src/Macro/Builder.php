<?php

namespace Rekamy\LaravelCore\Macro;

use Illuminate\Database\Query\Builder as Base;

class Builder extends Base
{

    public static function register()
    {
        return new static;
    }
    
    public function __construct()
    {
        static::macro('whereLike', function ($fields, $keyword, $useWildcard = true) {
            $this->where(function ($q) use ($fields, $keyword, $useWildcard) {
                if(is_string($fields)) $fields = [$fields];
                foreach ($fields as $index => $field) {
                    if($useWildcard) {
                        $q->orWhere($field, 'like', "%{$keyword}%");
                    } else {
                        $q->orWhere($field, 'like', $keyword);
                    }
                }
            });
            return $this;
        });
    }
}
