<?php

namespace Rekamy\LaravelCore\Criteria;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use Prettus\Repository\Contracts\RepositoryInterface;
use Prettus\Repository\Contracts\CriteriaInterface;
// use Illuminate\Pagination\LengthAwarePaginator;
use Exception;
use Rekamy\LaravelCore\Override\LengthAwarePaginator;

class DataTableCriteria implements CriteriaInterface
{
    /**
     * @var \Illuminate\Http\Request
     */
    protected $request;
    protected $resources;
    protected $appends;
    protected $query;
    protected $columns;
    protected $repository;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->appends = collect();
    }

    public function resolvePagination()
    {
        LengthAwarePaginator::currentPageResolver(function () {
            return $this->request->get('start') / $this->request->get('length') + 1;
        });
    }

    /**
     * Apply criteria in query repository
     *
     * @param         Builder|Model     $model
     * @param RepositoryInterface $repository
     *
     * @return mixed
     * @throws \Exception
     */
    public function apply($model, RepositoryInterface $repository)
    {
        if (!$this->request->has('draw')) return $model;
        $this->query = $model;
        $this->repository = $repository;

        if (!$this->request->has('columns')) throw new Exception('Column does not set', 400);
        $this->columns = collect($this->request->get('columns'))
            ->map(function ($value, $index) {
                $config = data_get($this->request, "config.columns.{$index}") ?? [];
                return [
                    'index' => $index,
                    ...$value,
                    ...$config
                ];
            })
            ->map(function ($value, $index) {
                $column = \Str::of($value['data']);
                if ($column->contains('.')) {
                    $columnName = $column->afterLast('.');
                    $relationName = $column->beforeLast('.');
                    $ref = (string) $relationName;
                    $tableName = $this->query->getModel()->$ref()->getRelated()->getTable();
                    $value['with'] = (string) $relationName;
                    // $value['columnAlias'] = (string) "{$tableName}.{$columnName} as {$tableName}-{$columnName}";
                    $value['column'] = (string) "{$tableName}-{$columnName}";
                } else if ($column->contains('-')) {
                    $columnName = $column->afterLast('-');
                    $aliasRelationName = (string) $column->beforeLast('-');
                    $relations = $column->beforeLast('-')->explode('-');
                    $refTableName = $this->query->getModel();
                    foreach ($relations as $relation) {
                        $modelRelation = $refTableName->$relation();
                        if (!$modelRelation instanceof \Illuminate\Database\Eloquent\Relations\Relation) {
                            break;
                        }
                        $refTableName = $modelRelation->getRelated();
                    }
                    $tableName = $refTableName->getTable();
                    $value['relation'] = (string) $aliasRelationName;
                    $value['columnAlias'] = (string) "{$aliasRelationName}.{$columnName} as {$aliasRelationName}-{$columnName}";
                    $value['column'] = (string) "{$aliasRelationName}-{$columnName}";
                    $value['fullQualifiedColumnAlias'] = (string) "{$aliasRelationName}.{$columnName}";
                } else {
                    $tableName = $this->query->getModel()->table;
                    $isVirtualAttribute = collect(explode(',', request('virtualColumns')))->intersect($column)->count() || data_get($value, 'virtual') === 'true';
                    if ($isVirtualAttribute) {
                        $value['column'] = $column;
                        $value['fullQualifiedColumnAlias'] = $column;
                    } else {
                        $value['columnAlias'] = (string) "{$tableName}.{$column}";
                        $value['column'] = (string) "{$tableName}.{$column}";
                        $value['fullQualifiedColumnAlias'] = (string) "{$tableName}.{$column}";
                    }
                }

                // $value['column'] = (string) "{$tableName}.{$columnName}";
                $value['table'] = (string) $tableName;
                return $value;
            });
        // dd($this->columns->pluck('columnAlias')->filter()->toArray());
        $this->query = $this->query->select($this->columns->pluck('columnAlias')->filter()->toArray());
        $this->loadAnyRelation();
        $this->applyQueryScope();
        $this->applyFilter();

        if ($this->isPaginatable()) $this->resolvePagination();

        $this->resources = $this->query->paginate($this->request->get('length'));

        return $this->resources;
    }

    public function loadAnyRelation()
    {
        $this->columns->pluck('relation')->filter()->unique()->each(function ($relationAlias) {
            // TODO: security feat: backend should filter queryable relation
            // $this->query = $this->query->leftJoinRelationship($relation);
            if (!str($relationAlias)->contains('-')) {
                $this->query = $this->query->leftJoinRelationshipUsingAlias($relationAlias, $relationAlias);
            } else {
                $nestedRelation = (string) str($relationAlias)->replace('-', '.');
                $relations = str($relationAlias)->explode('-');
                $aliases = [];
                foreach ($relations as $i => $relation) {
                    $aliases[] = [
                        'key' => $relation,
                        'callback' => function ($join) use ($relation) {
                            $join->as($relation);
                        }
                    ];
                }
                $aliases[sizeof($aliases) - 1]['callback'] = fn ($join) => $join->as($relationAlias);
                $aliases = collect($aliases)->keyBy('key')->map(fn ($item) => $item['callback'])->toArray();
                $this->query = $this->query->leftJoinRelationshipUsingAlias($nestedRelation, $aliases);
            }
        });
    }

    public function applyQueryScope()
    {
        if ($this->request->has('scope') || $this->request->has('scopes')) {
            $scopes = explode(',', $this->request->get('scope') ?? $this->request->get('scopes'));
            collect($scopes)->each(fn ($scope) => $this->query = $this->query->$scope());
        }
    }

    public function applyFilter()
    {
        if ($this->request->has('search') && !empty($keyword = $this->request->get('search')) && $keyword['value']) {
            $dtSearchable = $this->columns->where('searchable', 'true')->pluck('fullQualifiedColumnAlias')->filter()->toArray();
            // dd($dtSearchable);
            // TODO: security feat: backend should filter searchable column
            // $searchable = array_intersect($this->repository->getFieldsSearchable(), $dtSearchable);
            $this->query = $this->query->whereLike($dtSearchable, $keyword['value']);
        }


        if ($this->columns->where('search.value')->where('searchable', 'true')->count()) {
            $this->columns
                ->where('search.value')
                ->where('searchable', 'true')
                ->map(fn ($item) => [
                    'column' => $item['fullQualifiedColumnAlias'],
                    'keyword' => $item['search']['value'],
                    'useLike' => $item['search']['regex'] === 'true',
                ])->each(function ($search) {
                    if ($search['useLike']) {
                        $this->query = $this->query->where($search['column'], 'like', "%{$search['keyword']}%");
                    } else {
                        $this->query = $this->query->where($search['column'], "{$search['keyword']}");
                    }
                });
        }

        if ($this->request->has('order') && $this->columns->where('orderable', 'true')->count()) {
            $orders = $this->request->get('order');
            foreach ($orders as $sort) {
                $sortable = $this->columns->where('index', $sort['column'])->where('orderable', 'true')
                    ->where('fullQualifiedColumnAlias');
                $sortable->each(function ($column) use ($sort) {
                    $this->query = $this->query->orderBy($column['column'], $sort['dir']);
                });
            }
        }
    }

    /**
     * Check if Request allow pagination.
     *
     * @return bool
     */
    public function isPaginatable()
    {
        return !is_null($this->request->input('start')) &&
            !is_null($this->request->input('length')) &&
            $this->request->input('length') != -1;
    }
}
