<?php

namespace Rekamy\LaravelCore\Traits;

trait GroupedRecord
{
    public function scopeGroupedRecord($query, $field)
    {
        $class = \Str::of(class_basename(static::class))->plural();
        $relation = (string) \Str::of("{$class}-by-{$field}")->camel();
        $groupName = (string) \Str::of($relation)->camel();
        $query->select($field)->with($groupName)->groupBy($field);
    }
}
