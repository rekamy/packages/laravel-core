<?php

namespace Rekamy\LaravelCore\Crudable\Abstract;

use Rekamy\LaravelCore\Contracts\CrudableBloc as CrudableBlocInterface;
use Rekamy\LaravelCore\Crudable\Concern\CrudableBloc;
use Rekamy\LaravelCore\Crudable\Concern\HasRepository;
use Rekamy\LaravelCore\Crudable\Concern\HasRequest;

abstract class CrudBloc implements CrudableBlocInterface
{
    use HasRepository, HasRequest, CrudableBloc;

    protected $moduleName;

}
